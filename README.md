
## Caddy Elastic

![CI/CD status](https://gitlab.com/Kalkran/caddy-elastic/badges/main/pipeline.svg)

This project combines the [Caddy](https://caddyserver.com/) image with the [elastic](github.com/firecow/caddy-elastic-encoder) plugin, to make it easier to stay up to date with the `latest` Caddy Docker image and facilitates easier logging into Elasticsearch. The docker image is updated daily and published to this repositories container registry.

The `.gitlab-ci.yml` in this repository is the standard template provided by Gitlab. The Dockerfile is based on an example by Caddy.

#### References

* https://caddyserver.com/
* https://hub.docker.com/_/caddy (Under the heading `Adding custom Caddy modules`)
* https://github.com/firecow/caddy-elastic-encoder
* https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-input-filestream.html#_ndjson

#### Example configuration

##### docker-compose
```yaml
version: '3'

services:
  caddy:
    image: registry.gitlab.com/kalkran/caddy-elastic:latest
    container_name: caddy
    hostname: caddy

    restart: unless-stopped

    volumes:
      - /opt/caddy/Caddyfile:/etc/caddy/Caddyfile
      - /opt/caddy/data:/data
      - /opt/caddy/config:/config
      - /opt/caddy/logs:/srv

    ports:
      - "80:80"
      - "443:443"
```

##### filebeat.yml
```yaml
filebeat.inputs:
- type: filestream
  enabled: true
  paths:
    - /opt/caddy/logs/*.log

  parsers:
    - ndjson:
        keys_under_root: true
        expand_keys: true
```

#### Caddyfile
```
(logging) {
    log {
        format elastic
        output file /srv/access.log
    }

}

mydomain {
    import logging
    reverse_proxy my_server:80
}
```
