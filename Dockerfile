FROM caddy:2-builder AS builder

RUN xcaddy build \
    --with github.com/caddyserver/transform-encoder \
    --with github.com/Kalkran/caddy-elastic-encoder
    # --with github.com/firecow/caddy-elastic-encoder

FROM caddy:2

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
